import React, { Component } from "react";

export default class DemoState extends Component {
  // state: nơi chứa các variable liên quan đến quá trình re-render
  state = {
    like: 100,
    userName: "Alice",
  };
  handlePlusLike = () => {
    // setState : dùng để update giá trị của state
    // sau khi setState , render() sẽ chạy lại
    this.setState({ like: this.state.like + 1 });
  };

  handleChangeName = (newName) => {
    this.setState({
      userName: newName,
    });
  };

  render() {
    console.log("re render");
    return (
      <div>
        <span className="display-4 text-secondary">{this.state.like}</span>
        <button onClick={this.handlePlusLike} className="btn btn-success">
          Plus like
        </button>
        <br />
        <h2>{this.state.userName}</h2>
        <button
          className="btn btn-danger"
          onClick={() => {
           // đối với function có tham số thì dùng arrow function bọc lại
            this.handleChangeName("Tomy");
          }}
        >
          Change name
        </button>
      </div>
    );
  }
}
