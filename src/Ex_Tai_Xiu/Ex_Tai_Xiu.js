import React, { Component } from "react";
import bg_game from "../assets/bgGame.png";
import KetQua from "./KetQua";
import XucXac from "./XucXac";
import "./game.css";
export default class Ex_Tai_Xiu extends Component {
  render() {
    return (
      <div
        style={{
          backgroundImage: `url(${bg_game})`,
          width: "100vw",
          height: "100vh",
        }}
        className="bg_game"
      >
        <XucXac />
        <KetQua />
      </div>
    );
  }
}
// 16
