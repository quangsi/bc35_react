import React, { Component } from "react";

export default class DetailShoe extends Component {
  render() {
    let { name, id, price, description, image } = this.props.detail;
    console.log(" this.props: ", this.props);
    return (
      <div className="row mt-5 alert-secondary p-5 text-left">
        <img src={image} alt="" className="col-3" />
        <div className="col-9">
          <p>ID : {id} </p>
          <h5>Name :{name} </h5>
          <p>Desc :{description} </p>
          <p>Pirce :{price} </p>
        </div>
      </div>
    );
  }
}
