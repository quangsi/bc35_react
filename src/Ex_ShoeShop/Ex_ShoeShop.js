import React, { Component } from "react";
import Cart from "./Cart";
import { dataShoe } from "./dataShoe";
import DetailShoe from "./DetailShoe";
import ItemShoe from "./ItemShoe";
import ListShoe from "./ListShoe";

export default class Ex_ShoeShop extends Component {
  state = {
    shoeArr: dataShoe,
    detail: dataShoe[0],
    cart: [],
  };
  handleChangeDetailShoe = (shoe) => {
    this.setState({
      detail: shoe,
    });
  };
  handleAddToCart = (shoe) => {
    console.log("shoe: ", shoe);
    let cloneCart = [...this.state.cart];
    // TH1: chưa có sản phẩm trong giỏ hàng
    // TH2: sp đã có trong giỏ hàng
    let index = this.state.cart.findIndex((item) => {
      return item.id == shoe.id;
    });
    if (index == -1) {
      // sp ko có trong giỏi hàng => thêm mới
      let cartItem = { ...shoe, number: 1 };

      cloneCart.push(cartItem);
    } else {
      // sp đã có trong giỏ hàng

      cloneCart[index].number++;
    }
    this.setState({ cart: cloneCart });
  };
  // componentDidMount ~ life cycle
  render() {
    console.table(this.state.shoeArr);
    return (
      <div className="container py-5">
        <Cart cart={this.state.cart} />
        {/* <div className="row">{this.renderListShoe()}</div> */}
        <ListShoe
          handleAddToCart={this.handleAddToCart}
          handleChangeDetailShoe={this.handleChangeDetailShoe}
          shoeArr={this.state.shoeArr}
        />
        <DetailShoe detail={this.state.detail} />
      </div>
    );
  }
}
let a = 2;

let b = a;
let c = b;

// let color= ["blue","red"]
// color.pus("white")

// let newColors=[...color,"white"]
