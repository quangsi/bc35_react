import React, { Component } from "react";

export default class Cart extends Component {
  renderTbody = () => {
    return this.props.cart.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>
            <img
              style={{
                width: "50px",
              }}
              src={item.image}
              alt=""
            />
          </td>
          <td>{item.price * item.number} $</td>
          <td>
            <button className="btn btn-danger">-</button>
            <span className="mx-2">{item.number}</span>
            <button className="btn btn-success">+</button>
          </td>
        </tr>
      );
    });
  };

  render() {
    return (
      <table className="table">
        <thead>
          <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Img</th>
            <th>Price</th>
            <th>Quantity ( số lượng )</th>
          </tr>
        </thead>
        <tbody>{this.renderTbody()}</tbody>
      </table>
    );
  }
}
