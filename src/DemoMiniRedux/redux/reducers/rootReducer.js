import { combineReducers } from "redux";
import { numberReducer } from "./numberReducer";

export const rootReducer_DemoMiniRedux = combineReducers({
  soLuong: numberReducer,
});
