import React, { Component } from "react";
import { connect } from "react-redux";
import { GIAM_SO_LUONG, TANG_SO_LUONG } from "./redux/constants/numberConstant";

class DemoMiniRedux extends Component {
  render() {
    console.log(this.props);
    return (
      <div className="text-center pt-5 display-4">
        <button
          onClick={this.props.handleGiamSoLuong}
          className="btn btn-danger"
        >
          -
        </button>
        <span className=" mx-5">{this.props.count}</span>
        <button
          onClick={this.props.handleTangSoLuong}
          className="btn btn-success"
        >
          +
        </button>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    count: state.soLuong.number,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    handleTangSoLuong: () => {
      let action = {
        type: TANG_SO_LUONG,
      };
      dispatch(action);
    },
    handleGiamSoLuong: () => {
      dispatch({
        type: GIAM_SO_LUONG,
        payload: 10,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DemoMiniRedux);

let myMapDispatch = (dispatch) => {
  let action = {
    type: "tangSoLuong",
  };
  dispatch(action);
};

let sayHello = (data) => {
  // console.log(data.type);
};
myMapDispatch(sayHello);
// callback function

let sum = (a, b) => {
  return a + b;
};
sum(2, 1);

let a = 2;
let b = a;
let c = b;
