import React, { Component } from "react";
import { PureComponent } from "react";

export default class Header extends PureComponent {
  componentDidMount() {
    let count = 0;
    this.myCountDown = setInterval(() => {
      count++;
      // console.log("count down - Header", count);
    }, 1000);
  }
  render() {
    console.log("Header render");
    return (
      <div className="bg-dark text-white p-5">
        <p>Header</p>
        {/* <h2>Like: {this.props.like}</h2> */}
        <button onClick={this.props.handleClick} className="btn btn-danger">
          Plus like
        </button>
      </div>
    );
  }

  componentWillUnmount() {
    console.log("Header _ componentWillUnmount ( sắp được xoá khỏi giao diện");
    clearInterval(this.myCountDown);
  }
}
// PureComponent hạn chế render ko cần thiết

let cat = {
  name: "alice",
};
cat.age = 2;
