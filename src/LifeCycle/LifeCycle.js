import React, { Component } from "react";
import Header from "./Header/Header";

export default class LifeCycle extends Component {
  constructor(props) {
    super(props);
    this.inputRef = React.createRef();
  }
  state = {
    like: 1,
  };
  componentDidMount() {
    this.inputRef.current.value = "Have a good day";
    console.log("componentDidMount");
    // chỉ chạy 1 lần duy nhất sau lần render đầu tiên
    // thường để gọi api - check thông tin đăng nhập
  }
  shouldComponentUpdate(nextProps, nextState) {
    // nếu quên return hoặc return false thì ko render lại ~ mặc định sẽ return true
    console.log({ nextState });
    if (nextState.like == 5) {
      return false;
    } else {
      return true;
    }
  }
  handlePlusLike = () => {
    this.setState({ like: this.state.like + 1 });
  };
  render() {
    console.log("Lifecyle render");
    return (
      <div>
        {this.state.like < 5 && <Header handleClick={this.handlePlusLike} />}
        <span className="display-4">{this.state.like}</span>
        <button onClick={this.handlePlusLike} className="btn btn-success">
          Plus like
        </button>
        <br />
        <input ref={this.inputRef} type="text" />
      </div>
    );
  }
  componentDidUpdate(prePops, preState) {
    console.log("preState", preState);
    console.log("componentDidUpdate - LifeCycle");
    // tự động được chạy sau khi render chạy
  }
}
