import React, { Component } from "react";
import styles from "./navigate.module.css";
export default class Navigate extends Component {
  render() {
    return (
      <div className="p-5 bg-dark navigate">
        <p className={styles.navigate}>Navigate</p>
      </div>
    );
  }
}
