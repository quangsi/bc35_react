import React, { Component } from "react";

export default class UserInfor extends Component {
  render() {
    console.log(this.props);
    return (
      <div className="bg-warning">
        <h3>UserInfor</h3>
        <p>Name : {this.props.name}</p>
        <p>Age: {this.props.number}</p>
        <button
          onClick={() => {
            this.props.handleOnclick("Toomy");
          }}
          // anfn
          className="btn btn-success"
        >
          Change name
        </button>
      </div>
    );
  }
}
