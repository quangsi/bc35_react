import React, { Component } from "react";
import Ex_Layout_1 from "../Ex_Layout_1/Ex_Layout_1";
import Button from "./Button";
import UserInfor from "./UserInfor";

export default class DemoProps extends Component {
  state = {
    username: "Alice",
    age: 2,
  };
  handleChangeName = (name) => {
    console.log("yes");
    this.setState({ username: name, age: 100 });
  };
  render() {
    return (
      <div className="bg-primary p-5">
        <h2>DemoProps</h2>
        <UserInfor
          number={this.state.age}
          name={this.state.username}
          handleOnclick={this.handleChangeName}
        />
        <Button text={"Đăng nhập"} />
        <Button text={"Đăng xuất"} />
      </div>
    );
  }
}
