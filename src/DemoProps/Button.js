import React, { Component } from "react";
import styles from "./button.module.css";
export default class Button extends Component {
  render() {
    return (
      <div className={`${styles.button} btn btn-success ${styles.m10}`}>
        {this.props.text}
      </div>
    );
  }
}
