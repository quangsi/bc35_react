import { dataShoe } from "../../dataShoe";
import * as actionType from "../constants/shoeContant";
let initialState = {
  shoeArr: dataShoe,
  detail: dataShoe[3],
  cart: [],
};
export const shoeReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionType.ADD_TO_CART: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        return item.id == action.payload.id;
      });
      if (index == -1) {
        let cartItem = { ...action.payload, number: 1 };
        cloneCart.push(cartItem);
      } else {
        cloneCart[index].number++;
      }
      // state.cart = cloneCart;
      // return { ...state };
      return { ...state, cart: cloneCart };
    }
    case actionType.TANG_GIAM_SO_LUONG: {
      console.log(action);
      let cloneCart = [...state.cart];

      let index = cloneCart.findIndex((item) => {
        return item.id == action.payload.idShoe;
      });
      if (index !== -1) {
        cloneCart[index].number =
          cloneCart[index].number + action.payload.soLuong;
      }
      // if (cloneCart[index].number == 0) {
      //   cloneCart.splice(index, 1);
      // }
      cloneCart[index].number == 0 && cloneCart.splice(index, 1);
      return { ...state, cart: cloneCart };
    }
    case actionType.XEM_CHI_TIET: {
      return { ...state, detail: action.payload };
    }
    default:
      return state;
  }
};
// TANG_GIAM_SO_LUONG
