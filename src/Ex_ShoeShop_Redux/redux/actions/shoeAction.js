// action creator

import { XEM_CHI_TIET } from "../constants/shoeContant";

export const changDetailAction = (value) => {
 // axios
  return {
    type: XEM_CHI_TIET,
    payload: value,
  };
};
