import React, { Component } from "react";
import CardMovie from "./CardMovie";
import { dataFilm } from "./dataFilm";

export default class RenderWithMap extends Component {
  renderDanhSachPhim = () => {
    return dataFilm.map((item, index) => {
      return <CardMovie movie={item} key={index} />;
    });
  };
  render() {
    return (
      <div className="row container mt-5 mx-auto">
        {this.renderDanhSachPhim()}
      </div>
    );
  }
}
/**
 *  {
      maPhim: 1283,
      tenPhim: "Trainwreck",
      biDanh: "trainwreck",
      trailer: "https://www.youtube.com/embed/2MxnhBPoIx4",
      hinhAnh: "https://movienew.cybersoft.edu.vn/hinhanh/trainwreck.jpg",
      moTa: "Having thought that monogamy was never possible, a commitment-phobic career woman may have to face her fears when she meets a good guy.",
    maNhom: "GP00",
    ngayKhoiChieu: "2019-07-29T00:00:00",
    danhGia: 5,
  },
 */
