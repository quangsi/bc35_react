import logo from "./logo.svg";
import "./App.css";
import Demo_Class from "./Demo_Component/Demo_Class";
import Demo_Funcion from "./Demo_Component/Demo_Funcion";
import Ex_Layout_1 from "./Ex_Layout_1/Ex_Layout_1";
import DemoState from "./DemoState/DemoState";
import Data_Binding from "./Data_Binding/Data_Binding";
import DemoProps from "./DemoProps/DemoProps";
import RenderWithMap from "./RenderWithMap/RenderWithMap";
import Ex_ShoeShop from "./Ex_ShoeShop/Ex_ShoeShop";
import DemoMiniRedux from "./DemoMiniRedux/DemoMiniRedux";
import Ex_ShoeShop_Redux from "./Ex_ShoeShop_Redux/Ex_ShoeShop_Redux";
import Ex_Tai_Xiu from "./Ex_Tai_Xiu/Ex_Tai_Xiu";
import LifeCycle from "./LifeCycle/LifeCycle";
import Ex_Form from "./Ex_Form/Ex_Form";

function App() {
  return (
    <div className="App">
      {/* <Demo_Class /> */}
      {/* <Demo_Class></Demo_Class> */}
      {/* <Demo_Funcion /> */}

      {/* xử dụng */}

      {/* <Ex_Layout_1 /> */}

      {/* <DemoState /> */}
      {/* <DemoProps /> */}
      {/* <Data_Binding /> */}
      {/* <RenderWithMap /> */}
      {/* <Ex_ShoeShop /> */}
      {/* <DemoMiniRedux /> */}
      {/* <Ex_ShoeShop_Redux /> */}
      {/* <Ex_Tai_Xiu /> */}
      {/* <LifeCycle /> */}
      <Ex_Form />
    </div>
  );
}
// js ~ ts , jsx ~ tsx
export default App;
